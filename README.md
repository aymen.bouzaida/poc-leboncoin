POC-Leboncoin

Description

POC-Leboncoin is an iOS application developed using UIKit and Combine. The project follows the Single Responsibility Principle and Clean Architecture with the MVVM (Model-View-ViewModel) design pattern to ensure maintainability, scalability, and testability.

This project was created as a technical test to demonstrate proficiency in iOS development and software design principles.

• **Clean and modular architecture:** Designed with a focus on separation of concerns and ease of maintenance.  
• **MVVM design pattern:** Ensures clear separation between business logic and presentation layers.  
• **Asynchronous programming with Combine:** Utilizes Combine framework for reactive and declarative data flow.  
• **Adherence to Single Responsibility Principle:** Each component focuses on a single task to improve code clarity and reusability.  
• **No third-party libraries:** Developed without reliance on external frameworks or dependencies.  
• **Unit test coverage:** Achieves 81% code coverage for thorough testing.  