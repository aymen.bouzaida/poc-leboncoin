//
//  GetCategoryRepository.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

class GetCategoriesRepository: GetCategoriesRepositoryProtocol {
    var resources: Network
    var transformer: CategoriesTransformerProtocol
    
    init(resources: Network = NetworkAPI(), transformer: CategoriesTransformerProtocol = CategoriesTransformer()) {
        self.resources = resources
        self.transformer = transformer
    }
     
    func getCategoires() async -> Result<[CategoryModel], any Error> {
        do{
            let resultJson = try await resources.downloadApiData(.getCategories)
            let result = transformer.buildCategoriesModel(json: resultJson)
            return Result.success(result)
        } catch{
            return Result.failure(error)
        }
    }
}
