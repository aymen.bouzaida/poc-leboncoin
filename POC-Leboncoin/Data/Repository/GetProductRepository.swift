//
//  GetProductRepository.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

class GetProductRepository: GetProductRepositoryProtocol {
    var resources: Network
    var transformer: ProductTransformerProtocol
    
    init(resources: Network = NetworkAPI(), transformer: ProductTransformerProtocol = ProductTransformer()) {
        self.resources = resources
        self.transformer = transformer
    }
     
    func getProducts() async -> Result<[ProductModel], Error> {
        do{
            let resultJson = try await resources.downloadApiData(.getProducts)
            let result = transformer.buildProductModel(json: resultJson)
            return Result.success(result)
        } catch {
            return Result.failure(error)
        }
    }
}
