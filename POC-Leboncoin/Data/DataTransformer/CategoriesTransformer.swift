//
//  CategoriesTransformer.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

protocol CategoriesTransformerProtocol {
    func buildCategoriesModel(json: [CategoryJson]) -> [CategoryModel]
}

struct CategoriesTransformer: CategoriesTransformerProtocol {
    
    func buildCategoriesModel(json: [CategoryJson]) -> [CategoryModel] {
        json.map { CategoryModel(id: $0.id, name: $0.name) }
    }
}
