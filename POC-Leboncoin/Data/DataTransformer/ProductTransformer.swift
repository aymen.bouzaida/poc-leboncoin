//
//  ProductTransformer.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

protocol ProductTransformerProtocol {
    func buildProductModel(json: [ProductJson]) -> [ProductModel]
}

struct ProductTransformer: ProductTransformerProtocol {
  
    func buildProductModel(json: [ProductJson]) -> [ProductModel] {
        json.map { ProductModel(id: $0.id, categoryID: $0.categoryID, title: $0.title, description: $0.description, price: $0.price, imagesURL: $0.imagesURL.map { ImagesURLModel(small: $0.small,thumb: $0.thumb) }, creationDate: $0.creationDate?.formattedDate(), isUrgent: $0.isUrgent, siret: $0.siret) }
    }
}
