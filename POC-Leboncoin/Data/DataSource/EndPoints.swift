//
//  EndPoint.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

enum EndPoints: String {
    case GET_PRODUCTS = "/listing.json"
    case GET_CATEGORIES = "/categories.json"
}
