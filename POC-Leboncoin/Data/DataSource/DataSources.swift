//
//  DataSources.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

extension Resources {
    
    static var getProducts: Resources<[ProductJson]> {
        Resources<[ProductJson]>(
            url: URL(string: Self.baseUrl.absoluteString + EndPoints.GET_PRODUCTS.rawValue)!,
            method: HTTPMethod.GET
        )
    }
    
    static var getCategories: Resources<[CategoryJson]> {
        Resources<[CategoryJson]>(
            url: URL(string: Self.baseUrl.absoluteString + EndPoints.GET_CATEGORIES.rawValue)!,
            method: HTTPMethod.GET
        )
    }
}
