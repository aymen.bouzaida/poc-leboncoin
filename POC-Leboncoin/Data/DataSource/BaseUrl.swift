//
//  BaseUrl.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

extension Resources{
    static var baseUrl:URL{
        return URL(string: "https://raw.githubusercontent.com/leboncoin/paperclip/master")!
    }
}
