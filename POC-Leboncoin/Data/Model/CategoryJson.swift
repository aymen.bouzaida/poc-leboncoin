//
//  CategoryJson.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

struct CategoryJson: Codable {
    let id: Int?
    let name: String?
}
