//
//  ProductJson.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

struct ProductJson: Codable {
    let id, categoryID: Int?
    let title, description: String?
    let price: Int?
    let imagesURL: ImagesURL?
    let creationDate: String?
    let isUrgent: Bool?
    let siret: String?

    enum CodingKeys: String, CodingKey {
        case id
        case categoryID = "category_id"
        case title, description, price
        case imagesURL = "images_url"
        case creationDate = "creation_date"
        case isUrgent = "is_urgent"
        case siret
    }
}

struct ImagesURL: Codable {
    let small, thumb: String?
}
