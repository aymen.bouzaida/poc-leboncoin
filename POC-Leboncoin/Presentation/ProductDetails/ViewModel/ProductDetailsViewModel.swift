//
//  ProductDetailsViewModel.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 14/06/2024.
//

import Combine

class ProductDetailsViewModel {
    
    private let builder = ProductDetailsBuilder()
    private let product: ProductModel
    private let categoryName: String
    
    @Published var productDisplay: ProductDisplayModel?
    
    init(product: ProductModel, categoryName: String) {
        self.product = product
        self.categoryName = categoryName
    }
    
    func loadData() {
        productDisplay = builder.buildProductDisplay(productModel: product, categoryName: categoryName)
    }
}
