//
//  ProductDisplayModel.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 14/06/2024.
//

import Foundation

struct ProductDisplayModel {
    let title: String
    let price: String
    let category: String
    let description: String
    let imageUrl: String
    let creationDate: String
    let isUrgent: Bool
}
