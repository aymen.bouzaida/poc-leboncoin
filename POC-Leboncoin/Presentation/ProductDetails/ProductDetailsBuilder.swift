//
//  ProductDetailsBuilder.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 14/06/2024.
//

import Foundation

struct ProductDetailsBuilder {
    
    func buildProductDisplay(productModel: ProductModel, categoryName: String) -> ProductDisplayModel {
        return ProductDisplayModel(title: productModel.title ?? "", price: "\(productModel.price ?? 0) €", category: categoryName, description: productModel.description ?? "", imageUrl: productModel.imagesURL?.thumb ?? "", creationDate:
                                    "Posted \(productModel.creationDate ?? "")", isUrgent: productModel.isUrgent ?? false)
    }
}
