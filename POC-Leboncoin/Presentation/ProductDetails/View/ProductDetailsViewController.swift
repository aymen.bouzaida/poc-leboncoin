//
//  ProductDetailsViewController.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 14/06/2024.
//

import UIKit
import Combine

class ProductDetailsViewController: UIViewController {
    
    //MARK: - Private Properties
    private let viewModel: ProductDetailsViewModel
    private var subscriptions = Set<AnyCancellable>()

    //MARK: - Private UI Components
    private lazy var productImageView: CachedImageView = {
        let imageView = CachedImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "image-missing")
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 10
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 3
        return label
    }()
    
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.textAlignment = .left
        label.textColor = .darkGray
        return label
    }()
    
    private lazy var categoryLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        return label
    }()
    
    private lazy var creationDateLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.textColor = .lightGray
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var urgentLabel: UILabel = {
        let label = UILabel()
        label.text = "Urgent"
        label.textAlignment = .left
        label.textColor = .orange
        label.font = UIFont.preferredFont(forTextStyle: .footnote)
        label.numberOfLines = 0
        label.isHidden = true
        return label
    }()
    
    // MARK: - Init
    init(viewModel: ProductDetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        setupUI()
        viewModel.loadData()
        
        viewModel.$productDisplay.receive(on: DispatchQueue.main)
            .sink { [unowned self] productDisplayModel in
                guard let productDisplayModel else { return }
                self.configureView(product: productDisplayModel)
            }.store(in: &subscriptions)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // MARK: - Private Methods
    private func configureView(product: ProductDisplayModel) {
        titleLabel.text = product.title
        priceLabel.text = product.price
        categoryLabel.text = product.category
        creationDateLabel.text = product.creationDate
        descriptionLabel.text = product.description
        if let urlObject = URL(string: product.imageUrl) {
            productImageView.loadImage(fromURL: urlObject, placeholderImage: "image-missing")
        }
        urgentLabel.isHidden = !product.isUrgent
    }
    
    let scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    let stackView : UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
   
    private func setupUI() {
        view.addSubview(scrollView)
        scrollView.addSubview(stackView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
            scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
            scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16)
        ])
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: scrollView.contentLayoutGuide.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.contentLayoutGuide.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
        self.navigationController?.navigationBar.tintColor = .black
        stackView.addArrangedSubview(productImageView)
        
        productImageView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.3).isActive = true
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(priceLabel)
        stackView.addArrangedSubview(categoryLabel)
        
        let horizontalStackView = UIStackView()
        horizontalStackView.axis = .horizontal
        horizontalStackView.spacing = 10
        horizontalStackView.alignment = .fill
        horizontalStackView.distribution = .fill
        
        horizontalStackView.addArrangedSubview(urgentLabel)
        horizontalStackView.addArrangedSubview(creationDateLabel)

        stackView.addArrangedSubview(horizontalStackView)
        stackView.addArrangedSubview(descriptionLabel)
    }
}
// MARK: - New Instance
extension ProductDetailsViewController {
    static func newInstance(productModel: ProductModel, categoryName: String) -> ProductDetailsViewController {
        ProductDetailsViewController(viewModel: ProductDetailsViewModel(product: productModel, categoryName: categoryName))
    }
}
