//
//  CategoriesBuilder.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

struct CategoriesBuilder {
    
    func buildDisplayModel(categories: [CategoryModel]) -> [CategoriesDisplayModel] {
        categories.map { CategoriesDisplayModel(id: $0.id ?? 0, categoryName: $0.name ?? "") }
    }
}
