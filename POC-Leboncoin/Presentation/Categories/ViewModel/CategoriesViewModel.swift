//
//  CategoriesViewModel.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Combine

class CategoriesViewModel {
    
    // MARK: - Private Properties
    private let builder = CategoriesBuilder()
    private let categoriesModel: [CategoryModel]
    
    @Published var categories:[CategoriesDisplayModel] = []
    
    init(categoriesModel: [CategoryModel]) {
        self.categoriesModel = categoriesModel
    }
    
    func loadData() {
        categories = builder.buildDisplayModel(categories: categoriesModel)
    }
}
