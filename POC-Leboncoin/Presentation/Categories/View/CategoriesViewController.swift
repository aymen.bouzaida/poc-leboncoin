//
//  CategoriesViewController.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import UIKit
import Combine

class CategoriesViewController: UIViewController {

    static let identifier = "ProductTableViewCell"
    
    // MARK: - Public propeties
    let viewModel: CategoriesViewModel
    var dataPublisher: PassthroughSubject<Int?, Never>?
    var defaultCategorySelected: Int?

    //MARK: - Outlet
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.autoresizingMask = [.flexibleHeight]
        tableView.backgroundColor = .clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: CategoriesViewController.identifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    
    // MARK: - Private propeties
    private var subscriptions = Set<AnyCancellable>()

    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        viewModel.loadData()
        
        viewModel.$categories
            .receive(on: DispatchQueue.main)
            .sink { [weak self] value in
                guard let self else { return }
                self.tableView.reloadData()
            }.store(in: &subscriptions)
    }
    
    // MARK: Init
    init(viewModel: CategoriesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - private methods
    private func setupTableView() {
        view.backgroundColor = .white
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
}

// MARK: - UITableViewDelegate
extension CategoriesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.categories.count
    }
}

// MARK: - UITableViewDataSource
extension CategoriesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = viewModel.categories[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: CategoriesViewController.identifier, for: indexPath)
        cell.selectionStyle = .none
        cell.textLabel?.text = data.categoryName

        if let defaultCategorySelected, defaultCategorySelected == data.id {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let defaultCategorySelected, defaultCategorySelected == viewModel.categories[indexPath.row].id {
            self.dataPublisher?.send(nil)
        } else {
            self.dataPublisher?.send(viewModel.categories[indexPath.row].id)
        }
        self.dismiss(animated: true)
    }
}

// MARK: - New Instance
extension CategoriesViewController {
    static func newInstance(categoriesModel: [CategoryModel]) -> CategoriesViewController {
        CategoriesViewController(viewModel: CategoriesViewModel(categoriesModel: categoriesModel))
    }
}
