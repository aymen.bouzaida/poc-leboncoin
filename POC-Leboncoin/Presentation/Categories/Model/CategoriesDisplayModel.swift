//
//  CategoriesDisplayModel.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

struct CategoriesDisplayModel {
    let id: Int
    let categoryName: String
}
