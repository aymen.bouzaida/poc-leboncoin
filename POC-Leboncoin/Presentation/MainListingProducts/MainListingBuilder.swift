//
//  MainListingBuilder.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

struct MainListingBuilder {
    
    func buildDisplayModel(products: [ProductModel], categories: [CategoryModel]) -> [MainListingModel] {
        products.map { product in
            let category = categories.first { $0.id == product.categoryID ?? 0 }
            return MainListingModel(id: product.id ?? 0, title: product.title ?? "", imageUrl: product.imagesURL?.small ?? "", price: "\(product.price ?? 0) €", categoryName: category?.name ?? "", categoryId: category?.id ?? 0, isUrgent: product.isUrgent ?? false, creationDate: product.creationDate ?? "")
        }
        .sorted { $0.isUrgent && !$1.isUrgent }
    }
}
