//
//  ViewController.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import UIKit
import Combine

class MainListingProductsViewController: UIViewController {
    
    //MARK: - Properties
    private let viewModel = MainListingProductsViewModel()
    private var subscriptions = Set<AnyCancellable>()
    var dataPublisher = PassthroughSubject<Int?, Never>()
    private var defaultCategorySelected: Int?
    
    //MARK: - Loading View State UI
    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .medium)
        activityIndicatorView.startAnimating()
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicatorView.isHidden = false
        return activityIndicatorView
    }()
    
    //MARK: - Idle View State UI
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.autoresizingMask = [.flexibleHeight]
        tableView.backgroundColor = .clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(ProductTableViewCell.self, forCellReuseIdentifier: ProductTableViewCell.identifier)
        tableView.separatorStyle = .none
        tableView.keyboardDismissMode = .onDrag
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    private lazy var searchView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    private lazy var filterButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .clear
        button.layer.borderColor = UIColor.white.cgColor
        let imageButton = UIImage(named: "categories")
        button.setImage(imageButton, for: .normal)
        button.tintColor = .black
        button.imageView?.contentMode = .scaleAspectFit
        button.addTarget(self, action: #selector(onFilterButtonTap(sender:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.searchBarStyle = .minimal
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.placeholder = "Rechercher"
        searchBar.delegate = self
        return searchBar
    }()
    
    // MARK: ERROR View State UI
    private lazy var errorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .lightGray
        let contentLabel = UILabel()
        contentLabel.translatesAutoresizingMaskIntoConstraints = false
        contentLabel.text = "Oups!! Error loading"
        view.addSubview(contentLabel)
        NSLayoutConstraint.activate([
            contentLabel.heightAnchor.constraint(equalToConstant: 80),
            contentLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            contentLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        view.isHidden = true
        return view
    }()
        
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadProducts()
        setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setupData() {
        // Binding load data
        viewModel.$state
            .receive(on: DispatchQueue.main)
            .sink { [weak self] value in
                guard let self else { return }
                self.tableView.isHidden = value != .idle && value != .filterEnabled
                self.errorView.isHidden = value != .error
                self.activityIndicatorView.isHidden = value != .loading
                switch value {
                case .loading:
                    self.activityIndicatorView.startAnimating()
                    break
                case .idle:
                    self.activityIndicatorView.stopAnimating()
                    self.tableView.reloadData()
                    break
                case .error:
                    break;
                case .filterEnabled:
                    self.tableView.reloadData()
                    break;
                }
            }.store(in: &subscriptions)
        // Subscribe to the publisher
        dataPublisher.sink { [weak self] data in
            self?.viewModel.searchByCategory(id: data)
            self?.defaultCategorySelected = data
        }.store(in: &subscriptions)
    }
    
    func setupUI() {
        self.view.backgroundColor = .white
        self.navigationItem.title = ""
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
        setupLoadingView()
        setupErrorView()
        setupIdleView()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func setupLoadingView() {
        self.view.addSubview(activityIndicatorView)
        NSLayoutConstraint.activate([
            activityIndicatorView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
        ])
    }
    
    func setupErrorView() {
        self.view.addSubview(errorView)
        NSLayoutConstraint.activate([
            errorView.topAnchor.constraint(equalTo: self.view.topAnchor),
            errorView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            errorView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            errorView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
        ])
    }
    
    func setupIdleView() {
        view.addSubview(searchView)
        searchView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            searchView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            searchView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            searchView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor)
        ])
        
        searchView.addSubview(searchBar)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            searchView.heightAnchor.constraint(equalToConstant: 80),
            searchBar.topAnchor.constraint(equalTo: searchView.topAnchor),
            searchBar.leadingAnchor.constraint(equalTo: searchView.leadingAnchor),
            searchBar.bottomAnchor.constraint(equalTo: searchView.bottomAnchor),
        ])
        
        searchView.addSubview(filterButton)
        filterButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            filterButton.topAnchor.constraint(equalTo: searchView.topAnchor),
            filterButton.leadingAnchor.constraint(equalTo: searchBar.trailingAnchor),
            filterButton.bottomAnchor.constraint(equalTo: searchView.bottomAnchor),
            filterButton.trailingAnchor.constraint(equalTo: searchView.trailingAnchor, constant: -16),
            filterButton.widthAnchor.constraint(equalToConstant: 40),
        ])
        
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: self.searchView.bottomAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
        ])
    }
    
    @objc func onFilterButtonTap(sender: UIButton!) {
        let categoriesViewController = CategoriesViewController.newInstance(categoriesModel: viewModel.categoriesData)
        categoriesViewController.dataPublisher = self.dataPublisher
        categoriesViewController.defaultCategorySelected = self.defaultCategorySelected
        self.navigationController?.present(categoriesViewController, animated: true)
    }
    
    func loadProducts() {
        Task {
            await viewModel.loadProducts()
        }
    }
}

// MARK: - Table View Delegate & DataSource
extension MainListingProductsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.searchedProducts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductTableViewCell.identifier, for: indexPath) as? ProductTableViewCell else {
            return UITableViewCell()
        }
        guard let products = viewModel.searchedProducts else { return cell }
        let data = products[indexPath.row]
        cell.configure(product: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let products = viewModel.searchedProducts else { return }
        let selectedProductID = products[indexPath.row].id
        let selectedProductCategoryName = products[indexPath.row].categoryName
        if let productModel = viewModel.getProduct(with: selectedProductID) {
            let productDetailsViewController = ProductDetailsViewController(viewModel: ProductDetailsViewModel(product: productModel, categoryName: selectedProductCategoryName))
            self .navigationController?.show(productDetailsViewController, sender: self)
        }
    }
}

// MARK: - Search Bar Delegate
extension MainListingProductsViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        guard let searchText = searchBar.text else { return }
        viewModel.search(by: searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        searchBar.text = ""
        viewModel.search(by: "")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.search(by: searchText)
    }
}
