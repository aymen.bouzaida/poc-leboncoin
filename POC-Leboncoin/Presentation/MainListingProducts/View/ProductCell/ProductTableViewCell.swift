//
//  ProductTableViewCell.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import UIKit

class ProductTableViewCell: UITableViewCell {
    static let identifier = "ProductTableViewCell"
   
    private let cardView: UIView = {
        let view = UIView()
        view.layer.backgroundColor = UIColor.white.cgColor
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 8
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = 1
        view.layer.shouldRasterize = true
        return view
    }()
    
    private let productImageView : CachedImageView = {
        let imageView = CachedImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "image-missing")
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.textAlignment = .left
        label.font = .preferredFont(forTextStyle: .subheadline)
        label.text = "titre de produit"
        label.textColor = .black
        return label
    }()
    
    private let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.textAlignment = .left
        label.font = .preferredFont(forTextStyle: .footnote)
        label.text = "100$"
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let categoryLabel: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.textAlignment = .left
        label.font = .preferredFont(forTextStyle: .footnote)
        label.text = "Véhicule"
        label.textColor = .black
        return label
    }()
    
    private lazy var urgentView: UIView = {
        let label = UILabel()
        label.text = "Urgent"
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.layer.cornerRadius = 10
        label.layer.masksToBounds = true
        
        let containerView = UIView()
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        containerView.backgroundColor = .orange
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(label)
        
        let padding: CGFloat = 4.0
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: containerView.topAnchor, constant: padding),
            label.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: padding),
            label.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -padding),
            label.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -padding)
        ])
        return containerView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        self.contentView.backgroundColor = .white
        
        contentView.addSubview(cardView)
        cardView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            cardView.leftAnchor.constraint(equalTo: self.contentView.safeAreaLayoutGuide.leftAnchor, constant: 16),
            cardView.rightAnchor.constraint(equalTo: self.contentView.safeAreaLayoutGuide.rightAnchor, constant: -16),
            cardView.topAnchor.constraint(equalTo: self.contentView.safeAreaLayoutGuide.topAnchor, constant: 8),
            cardView.bottomAnchor.constraint(equalTo: self.contentView.safeAreaLayoutGuide.bottomAnchor, constant: -8),
        ])
        
        cardView.addSubview(productImageView)
        productImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            productImageView.leftAnchor.constraint(equalTo: cardView.leftAnchor, constant: 8),
            productImageView.rightAnchor.constraint(equalTo: cardView.safeAreaLayoutGuide.rightAnchor, constant: -8),
            productImageView.topAnchor.constraint(equalTo: cardView.safeAreaLayoutGuide.topAnchor, constant: 8),
            productImageView.heightAnchor.constraint(equalToConstant: 180)
        ])

        cardView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.leftAnchor.constraint(equalTo: cardView.safeAreaLayoutGuide.leftAnchor, constant: 8),
            titleLabel.rightAnchor.constraint(equalTo: cardView.safeAreaLayoutGuide.rightAnchor, constant: -8),
            titleLabel.topAnchor.constraint(equalTo: productImageView.bottomAnchor, constant: 8),
            titleLabel.heightAnchor.constraint(equalToConstant: 20)
        ])
        
        cardView.addSubview(priceLabel)
        NSLayoutConstraint.activate([
            priceLabel.leftAnchor.constraint(equalTo: cardView.leftAnchor, constant: 8),
            priceLabel.rightAnchor.constraint(equalTo: cardView.rightAnchor, constant: -8),
            priceLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            priceLabel.heightAnchor.constraint(equalToConstant: 20)
        ])
        
        cardView.addSubview(categoryLabel)
        categoryLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            categoryLabel.leftAnchor.constraint(equalTo: cardView.leftAnchor, constant: 8),
            categoryLabel.rightAnchor.constraint(equalTo: cardView.rightAnchor, constant: -8),
            categoryLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor),
            categoryLabel.heightAnchor.constraint(equalToConstant: 20),
            categoryLabel.bottomAnchor.constraint(equalTo: cardView.bottomAnchor, constant: -8)
        ])
        cardView.addSubview(urgentView)
        NSLayoutConstraint.activate([
            urgentView.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 16),
            urgentView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 16)
        ])
    }
    
    public func configure(product: MainListingModel) {
        if let urlObject = URL(string: product.imageUrl) {
            self.productImageView.loadImage(fromURL: urlObject, placeholderImage: "image-missing")
        }
        self.titleLabel.text = product.title
        self.priceLabel.text = product.price
        self.categoryLabel.text = product.categoryName
        self.urgentView.isHidden = !product.isUrgent
    }
}
