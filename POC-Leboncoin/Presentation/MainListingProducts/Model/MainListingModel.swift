//
//  MainListingModel.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

struct MainListingModel {
    let id: Int
    let title: String
    let imageUrl: String
    let price: String
    let categoryName: String
    let categoryId: Int
    let isUrgent: Bool
    let creationDate: String
}

// MARK: - Hashable
// used to compare two object of MainListingModel
extension MainListingModel: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
