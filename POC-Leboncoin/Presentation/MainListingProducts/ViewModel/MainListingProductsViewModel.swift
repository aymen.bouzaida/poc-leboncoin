//
//  MainListingProductsViewModel.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation
import Combine

enum MainListingProductsState {
    case loading
    case idle
    case error
    case filterEnabled
}

class MainListingProductsViewModel {
    
    // MARK: - Private Properties
    private var productsData: [ProductModel] = []
    private(set) var categoriesData: [CategoryModel] = []
    private let useCaseGetProducts: ProductsUseCaseProtocol
    private let useCaseGetCategories: CategoriesUseCaseProtocol
    private let builder = MainListingBuilder()
    
    @Published var state: MainListingProductsState = .loading
    
    var products: [MainListingModel]? {
        willSet {
            searchedProducts = newValue
        }
    }
    var searchedProducts: [MainListingModel]?
    
    init(useCaseGetProducts: ProductsUseCaseProtocol = ProductsUseCase(), useCaseGetCategories: CategoriesUseCaseProtocol = CategoriesUseCase()) {
        self.useCaseGetProducts = useCaseGetProducts
        self.useCaseGetCategories = useCaseGetCategories
    }
    
    func loadProducts() async {
        let data = await useCaseGetProducts.fetchProducts()
        state = .loading
        switch data {
        case .success(let products):
            
            self.productsData = products
            await loadCategories()
            break;
        case .failure:
            self.state = .error
            self.productsData = []
            self.products = []
            break;
        }
    }
    
    private func loadCategories() async {
        let categories = await useCaseGetCategories.fetchCateogires()
        switch categories {
        case .success(let categories):
            self.categoriesData = categories
            self.products = builder.buildDisplayModel(products: productsData, categories: categoriesData)
            self.state = .idle
            break;
        case .failure:
            self.state = .error
            self.categoriesData = []
            break;
        }
    }
    
    func search(by word: String) {
        if word.isEmpty {
            state = .idle
            searchedProducts = products
        } else {
            searchedProducts = products?.filter { $0.title.contains(word) }
            state = .filterEnabled
        }
    }
    
    func searchByCategory(id: Int?) {
        guard let id else {
            searchedProducts = products
            state = .idle
            return
        }
        searchedProducts = products?.filter { $0.categoryId == id }
        state = .filterEnabled
    }
    
    func getProduct(with id: Int) -> ProductModel? {
        return productsData.first { $0.id == id }
    }
}
