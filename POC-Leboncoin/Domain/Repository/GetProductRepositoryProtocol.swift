//
//  GetProductRepositoryProtocol.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

protocol GetProductRepositoryProtocol {
    func getProducts() async -> Result<[ProductModel], Error>
}
