//
//  GetCategoriesRepositoryProtocol.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

protocol GetCategoriesRepositoryProtocol {
    func getCategoires() async -> Result<[CategoryModel], Error>
}
