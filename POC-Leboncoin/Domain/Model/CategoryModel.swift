//
//  CategoryModel.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

struct CategoryModel {
    var id: Int?
    var name: String?
}
