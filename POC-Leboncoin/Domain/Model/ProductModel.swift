//
//  ProductModel.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

struct ProductModel {
    var id, categoryID: Int?
    var title, description: String?
    var price: Int?
    var imagesURL: ImagesURLModel?
    var creationDate: String?
    var isUrgent: Bool?
    var siret: String?
}

struct ImagesURLModel {
    var small, thumb: String?
}
