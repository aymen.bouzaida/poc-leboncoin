//
//  CategoriesUseCase.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

protocol CategoriesUseCaseProtocol {
    func fetchCateogires() async -> Result<[CategoryModel],Error>
}

public class CategoriesUseCase: CategoriesUseCaseProtocol {
  
    private var getCategoriesRepository: GetCategoriesRepositoryProtocol
    
    func fetchCateogires() async -> Result<[CategoryModel], any Error> {
        await getCategoriesRepository.getCategoires()
    }

    init(repository: GetCategoriesRepositoryProtocol = GetCategoriesRepository()) {
        self.getCategoriesRepository = repository
    }
}
