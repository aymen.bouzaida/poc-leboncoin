//
//  ProductsUseCase.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

protocol ProductsUseCaseProtocol {
    func fetchProducts() async -> Result<[ProductModel],Error>
}

public class ProductsUseCase: ProductsUseCaseProtocol {
  
    private var getProductsReporitory: GetProductRepositoryProtocol
    
    func fetchProducts() async -> Result<[ProductModel], any Error> {
        await getProductsReporitory.getProducts()
    }

    init(repository: GetProductRepositoryProtocol = GetProductRepository()) {
        self.getProductsReporitory = repository
    }
    
}
