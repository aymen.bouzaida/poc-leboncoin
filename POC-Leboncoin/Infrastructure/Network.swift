//
//  Network.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

protocol NetworkSession {
    func data(request: URLRequest) async throws -> (Data, URLResponse)
}

extension URLSession: NetworkSession {
    func data(request: URLRequest) async throws -> (Data, URLResponse) {
        let (data, response) = try await self.data(for: request)
        return (data, response)
    }
}

protocol Network {
    func downloadApiData<T: Codable>(_ resource: Resources<T>) async throws -> T
}

class NetworkAPI: Network {
    
    func downloadApiData<T: Codable>(_ resource: Resources<T>) async throws -> T {
        do {
            
            var request = URLRequest(url: resource.url)
            
            request.allHTTPHeaderFields = [
                "Content-Type": "application/json",
            ]
            
            request.httpMethod = resource.method.name
            
            let components = URLComponents(url: resource.url, resolvingAgainstBaseURL: true)
            guard let url = components?.url else {
                throw NetworkErrorInterceptor.badUrl(components?.url?.absoluteString)
            }
            request.url = url
            
            let configuraton = URLSessionConfiguration.default
            let session = URLSession(configuration: configuraton)
            let (data, response) = try await session.data(request: request)

            guard let httpResponse = response as? HTTPURLResponse else {
                throw NetworkErrorInterceptor.unknownError
            }
            
            if httpResponse.statusCode < 200 && httpResponse.statusCode > 300 {
                throw NetworkErrorInterceptor.unknownError
            }
            
            do {
                let result = try JSONDecoder().decode(T.self, from: data)
                return result
            } catch {
                throw NetworkErrorInterceptor.decodingError
            }
            
        } catch {
            guard let urlError = error as? URLError else {
                throw NetworkErrorInterceptor.unknownError
            }
            if urlError.code == .notConnectedToInternet {
                throw NetworkErrorInterceptor.noInternet
            } else if urlError.code == .timedOut {
                throw NetworkErrorInterceptor.connectionTimeout
            } else {
                throw NetworkErrorInterceptor.unknownError
            }
        }
    }
    
}

public enum NetworkErrorInterceptor: Error {
    case badUrl(String?)
    case noInternet
    case connectionTimeout
    case unknownError
    case decodingError
    case noData
    
    public var message: String {
        switch self {
        case .badUrl(let url): return "Invalid URL: \(String(describing: url))"
        case .decodingError: return "Data missmatch. Please try again!"
        case .unknownError: return "Unknown error occurred. Please try again"
        case .noInternet: return "No internet connection. Please check your internet connection"
        case .connectionTimeout: return "Connection timeout. Please try again"
        case .noData: return "No data found. Please try again"
        }
    }
}
