//
//  Resource.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import Foundation

struct Resources<T:Codable> {
    let url:URL
    let method: HTTPMethod
    
    init(url: URL, method: HTTPMethod) {
        self.url = url
        self.method = method
    }
}

enum HTTPMethod {
    
    case GET
    
    var name: String {
        switch self {
        case .GET: return "GET"
        }
    }
}

