//
//  CachedImageView.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 13/06/2024.
//

import UIKit

class CachedImageView: UIImageView {
    private static let imageCache = NSCache<AnyObject, UIImage>()
    
    func loadImage(fromURL imageURL: URL, placeholderImage: String) {
        self.image = UIImage(named: placeholderImage)
        
        if let cachedImage = CachedImageView.imageCache.object(forKey: imageURL as AnyObject) {
            self.image = cachedImage
            return
        }
        
        DispatchQueue.global().async { [weak self] in
            if let imageData = try? Data(contentsOf: imageURL) {
                if let image = UIImage(data: imageData) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
