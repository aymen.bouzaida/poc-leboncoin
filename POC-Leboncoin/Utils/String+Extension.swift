//
//  String+Extension.swift
//  POC-Leboncoin
//
//  Created by Aymen Bouzaida on 14/06/2024.
//

import Foundation

extension String {
    func formattedDate() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        // Optionally, set locale to user's current locale for localized formatting
        dateFormatter.locale = Locale.current
        
        guard let date = dateFormatter.date(from: self) else {
            return nil
        }
        
        // Create a new DateFormatter for localized output
        let localizedDateFormatter = DateFormatter()
        localizedDateFormatter.dateStyle = .medium
        localizedDateFormatter.timeStyle = .short
        localizedDateFormatter.locale = Locale.current // Use current locale for localized formatting
        
        return localizedDateFormatter.string(from: date)
    }
}
