//
//  ProductsUseCaseTests.swift
//  POC-LeboncoinTests
//
//  Created by Aymen Bouzaida on 15/06/2024.
//

import XCTest
@testable import POC_Leboncoin

final class ProductsUseCaseTests: XCTestCase {

    // Subject Under Test
    var sut: ProductsUseCase?
    
    // MARK: Life Cycle
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    // MARK: fetchProducts()
    func test_fetch_products_success() async {
        // Given
        sut = ProductsUseCase(repository: GetProductRepositoryMock_Success())
        // When
        let result = await sut?.fetchProducts()
        //Then
        switch result {
        case .success(let products):
            XCTAssertEqual(products.first?.id, 1)
            XCTAssertEqual(products.first?.categoryID, 1)
            XCTAssertEqual(products.first?.title, "produit1")
            XCTAssertEqual(products.first?.price, 200)
            XCTAssertEqual(products.first?.imagesURL?.small, "url1")
            XCTAssertEqual(products.first?.isUrgent, true)
        default:
            break;
        }
    }
    
    func test_fetch_products_failure() async {
        // Given
        sut = ProductsUseCase(repository: GetProductRepositoryMock_Failure())
        // When
        let result = await sut?.fetchProducts()
        //Then
        switch result {
        case .failure(let error as NetworkErrorInterceptor):
            XCTAssertEqual(error.message, "Data missmatch. Please try again!")
        default:
            break;
        }
    }
}

class GetProductRepositoryMock_Success: GetProductRepositoryProtocol{
    func getProducts() async -> Result<[POC_Leboncoin.ProductModel], any Error> {
        return .success([
            ProductModel(id: 1, categoryID: 1, title: "produit1", price: 200,imagesURL: ImagesURLModel(small: "url1") ,isUrgent: true),
            ProductModel(id: 2, categoryID: 2, title: "produit2", price: 200,imagesURL: ImagesURLModel(small: "url2") ,isUrgent: true),
            ProductModel(id: 3, categoryID: 3, title: "produit3", price: 200,imagesURL: ImagesURLModel(small: "url3") ,isUrgent: true),
            ProductModel(id: 4, categoryID: 3, title: "produit4", price: 200,imagesURL: ImagesURLModel(small: "url4") ,isUrgent: true),
        ])
    }
}


class GetProductRepositoryMock_Failure: GetProductRepositoryProtocol{
    func getProducts() async -> Result<[POC_Leboncoin.ProductModel], any Error> {
        return .failure(NetworkErrorInterceptor.decodingError)
    }
}
