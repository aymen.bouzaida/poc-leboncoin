//
//  CategoriesUseCase.swift
//  POC-LeboncoinTests
//
//  Created by Aymen Bouzaida on 14/06/2024.
//

import XCTest
@testable import POC_Leboncoin

final class CategoriesUseCaseTests: XCTestCase {

    // Subject Under Test
    var sut: CategoriesUseCase?
    
    // MARK: Life Cycle
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    // MARK: Func fetchCategories()
    func test_fetchCateogires_success() async throws {
        // Given
        sut = CategoriesUseCase(repository: GetCategoriesRepositoryMock())
        // When
        let result = await sut?.fetchCateogires()
        // Then
        switch result {
        case .success(let product):
            XCTAssertTrue(product.count == 3)
            XCTAssertTrue(product.first?.name == "category1")
        default:
            break
        }
    }
    
    func test_fetchCateogires_failed() async throws {
        // Given
        sut = CategoriesUseCase(repository: GetCategoriesRepositoryMockFailed())
        // When
        let result = await sut?.fetchCateogires()
        // Then
        switch result {
        case .failure(let error):
            let nsError = error as NSError
            XCTAssertEqual(nsError.code, 1)
            XCTAssertTrue(nsError.domain == "testError")
        default:
            break
        }
    }
}

class GetCategoriesRepositoryMock: GetCategoriesRepositoryProtocol {
    func getCategoires() async -> Result<[POC_Leboncoin.CategoryModel], any Error> {
        .success(
            [
                CategoryModel(id:1, name: "category1"),
                CategoryModel(id:2, name: "category2"),
                CategoryModel(id:3, name: "category3")
            ]
        )
    }
}

class GetCategoriesRepositoryMockFailed: GetCategoriesRepositoryProtocol {
    func getCategoires() async -> Result<[POC_Leboncoin.CategoryModel], any Error> {
        .failure(NSError(domain: "testError", code: 1))
    }
}
