//
//  CategoriesViewModelTests.swift
//  POC-LeboncoinTests
//
//  Created by Aymen Bouzaida on 15/06/2024.
//

import XCTest
@testable import POC_Leboncoin

final class CategoriesViewModelTests: XCTestCase {

    var sut: CategoriesViewModel?  // Subject under test
    
    // MARK: Life Cycle
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    // MARK: - Func  loadData()
    func test_loadData() {
        // Given
        sut = CategoriesViewModel(categoriesModel: [
            CategoryModel(id:1, name: "category1"),
            CategoryModel(id:2, name: "category2"),
            CategoryModel(id:3, name: "category3")
        ])
        // When
        sut?.loadData()
        // Then
        XCTAssertEqual(sut?.categories.count, 3)
        XCTAssertEqual(sut?.categories.first?.id, 1)
        XCTAssertEqual(sut?.categories.last?.id, 3)
        XCTAssertEqual(sut?.categories.first?.categoryName, "category1")
        XCTAssertEqual(sut?.categories.last?.categoryName, "category3")
    }
}
