//
//  ProductDetailsViewModelTests.swift
//  POC-LeboncoinTests
//
//  Created by Aymen Bouzaida on 15/06/2024.
//

import XCTest
@testable import POC_Leboncoin

final class ProductDetailsViewModelTests: XCTestCase {
    
    var sut: ProductDetailsViewModel?
    
    // MARK: Life Cycle
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    //MARK: Func Load Data
    func test_load_data() {
        // GIVEN$
        let categoryName = "categoryTest"
        sut = ProductDetailsViewModel(product: ProductModel(id: 1, categoryID: 1, title: "produit1", description: "product desciption test", price: 200,imagesURL: ImagesURLModel(thumb: "url1") ,creationDate: "2019-11-05T15:56:59+0000", isUrgent: true), categoryName: categoryName)
        // WHEN
        sut?.loadData()
        // THEN
        XCTAssertEqual(sut?.productDisplay?.title, "produit1")
        XCTAssertEqual(sut?.productDisplay?.category, categoryName)
        XCTAssertEqual(sut?.productDisplay?.imageUrl, "url1")
        XCTAssertEqual(sut?.productDisplay?.isUrgent, true)
        XCTAssertEqual(sut?.productDisplay?.price, "\(200) €")
        XCTAssertEqual(sut?.productDisplay?.description, "product desciption test")
    }
    
    func test_load_data_with_nil_inputs() {
        // GIVEN$
        let categoryName = "categoryTest"
        sut = ProductDetailsViewModel(product: ProductModel(id: 1, categoryID: 1, title: nil, description: nil, price: nil, imagesURL: nil, creationDate: nil, isUrgent: nil), categoryName: categoryName)
        // WHEN
        sut?.loadData()
        // THEN
        XCTAssertEqual(sut?.productDisplay?.title, "")
        XCTAssertEqual(sut?.productDisplay?.category, categoryName)
        XCTAssertEqual(sut?.productDisplay?.imageUrl, "")
        XCTAssertEqual(sut?.productDisplay?.isUrgent, false)
        XCTAssertEqual(sut?.productDisplay?.price, "0 €")
        XCTAssertEqual(sut?.productDisplay?.description, "")
    }
}
