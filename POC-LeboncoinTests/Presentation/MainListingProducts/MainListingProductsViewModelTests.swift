//
//  MainListingProductsViewModelTests.swift
//  POC-LeboncoinTests
//
//  Created by Aymen Bouzaida on 14/06/2024.
//

import XCTest
@testable import POC_Leboncoin

final class MainListingProductsViewModelTests: XCTestCase {
    
    // Subject under test
    var sut: MainListingProductsViewModel?
    
    // MARK: Life Cycle
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    // MARK: - Func  loadProducts
    func test_loadProducts_success_case() async throws {
        // Given
        sut = MainListingProductsViewModel(useCaseGetProducts: ProductsUseCaseMockSuccess(),
                                           useCaseGetCategories: CategoriesUseCaseMockSuccess())
        // When
        await sut?.loadProducts()
        // Then
        XCTAssertEqual(sut?.state, .idle)
        XCTAssertEqual(sut?.products?.count, 4)
        XCTAssertEqual(sut?.searchedProducts?.count, 4)
        XCTAssertEqual(sut?.products, sut?.searchedProducts)
        XCTAssertEqual(sut?.products?.first?.categoryName, "category1")
        XCTAssertEqual(sut?.products?.last?.categoryName, "category3")
    }
    
    func test_loadProducts_failed_case() async throws {
        // Given
        sut = MainListingProductsViewModel(useCaseGetProducts: ProductsUseCaseMockFailed(),
                                           useCaseGetCategories: CategoriesUseCaseMockFailed())
        // When
        await sut?.loadProducts()
        // Then
        XCTAssertEqual(sut?.state, .error)
        XCTAssertEqual(sut?.products?.count, 0)
        XCTAssertEqual(sut?.searchedProducts?.count, 0)
        XCTAssertEqual(sut?.products, sut?.searchedProducts)
        XCTAssertNil(sut?.products?.first?.categoryName)
    }
    
    func test_loadProducts_succes_and_load_categories_failed_case() async throws {
        // Given
        sut = MainListingProductsViewModel(useCaseGetProducts: ProductsUseCaseMockSuccess(),
                                           useCaseGetCategories: CategoriesUseCaseMockFailed())
        // When
        await sut?.loadProducts()
        // Then
        XCTAssertEqual(sut?.state, .error)
        XCTAssertNil(sut?.products)
        XCTAssertNil(sut?.searchedProducts)
        XCTAssertEqual(sut?.products, sut?.searchedProducts)
        XCTAssertNil(sut?.products?.first?.categoryName)
    }
    
    // MARK: - Func search
    func test_search_by_word_case1() async throws {
        // Given
        sut = MainListingProductsViewModel(useCaseGetProducts: ProductsUseCaseMockSuccess(),
                                           useCaseGetCategories: CategoriesUseCaseMockSuccess())
        await sut?.loadProducts()
        // When
        sut?.search(by: "produit1")
        // Then
        XCTAssertEqual(sut?.state, .filterEnabled)
        XCTAssertEqual(sut?.searchedProducts?.count, 1)
    }
    
    func test_search_by_word_case2() async throws {
        // Given
        sut = MainListingProductsViewModel(useCaseGetProducts: ProductsUseCaseMockSuccess(),
                                           useCaseGetCategories: CategoriesUseCaseMockSuccess())
        await sut?.loadProducts()
        // When
        sut?.search(by: "produit")
        // Then
        XCTAssertEqual(sut?.state, .filterEnabled)
        XCTAssertEqual(sut?.searchedProducts?.count, 4)
    }
    
    func test_search_by_empty_word() async throws {
        // Given
        sut = MainListingProductsViewModel(useCaseGetProducts: ProductsUseCaseMockSuccess(),
                                           useCaseGetCategories: CategoriesUseCaseMockSuccess())
        await sut?.loadProducts()
        // When
        sut?.search(by: "")
        // Then
        XCTAssertEqual(sut?.state, .idle)
        XCTAssertEqual(sut?.searchedProducts?.count, 4)
    }
    
    // MARK: - searchByCategory
    func test_search_by_category_case1() async throws {
        // Given
        sut = MainListingProductsViewModel(useCaseGetProducts: ProductsUseCaseMockSuccess(),
                                           useCaseGetCategories: CategoriesUseCaseMockSuccess())
        await sut?.loadProducts()
        // When
        sut?.searchByCategory(id: 2)
        // Then
        XCTAssertEqual(sut?.state, .filterEnabled)
        XCTAssertEqual(sut?.searchedProducts?.count, 1)
    }
    
    func test_search_by_category_case2() async throws {
        // Given
        sut = MainListingProductsViewModel(useCaseGetProducts: ProductsUseCaseMockSuccess(),
                                           useCaseGetCategories: CategoriesUseCaseMockSuccess())
        await sut?.loadProducts()
        // When
        sut?.searchByCategory(id: 3)
        // Then
        XCTAssertEqual(sut?.state, .filterEnabled)
        XCTAssertEqual(sut?.searchedProducts?.count, 2)
    }
    
    func test_search_by_category_nil() async throws {
        // Given
        sut = MainListingProductsViewModel(useCaseGetProducts: ProductsUseCaseMockSuccess(),
                                           useCaseGetCategories: CategoriesUseCaseMockSuccess())
        await sut?.loadProducts()
        // When
        sut?.searchByCategory(id: nil)
        // Then
        XCTAssertEqual(sut?.state, .idle)
        XCTAssertEqual(sut?.searchedProducts?.count, 4)
    }
    
    // MARK: - Get Products
    func test_getProduct_known_id() async throws {
        // Given
        let idProduct = 3
        sut = MainListingProductsViewModel(useCaseGetProducts: ProductsUseCaseMockSuccess(),
                                           useCaseGetCategories: CategoriesUseCaseMockSuccess())
        await sut?.loadProducts()
        // When
        let product = sut?.getProduct(with: idProduct)
        // Then
        XCTAssertTrue(product?.title == "produit3")
        XCTAssertEqual(product?.price, 200)
        XCTAssertEqual(product?.id, idProduct)
    }
    
    func test_getProduct_unknown_id() async throws {
        // Given
        let idProduct = 100
        sut = MainListingProductsViewModel(useCaseGetProducts: ProductsUseCaseMockSuccess(),
                                           useCaseGetCategories: CategoriesUseCaseMockSuccess())
        await sut?.loadProducts()
        // When
        let product = sut?.getProduct(with: idProduct)
        // Then
        XCTAssertNil(product)
    }
}


class ProductsUseCaseMockSuccess: ProductsUseCaseProtocol {
    func fetchProducts() async -> Result<[POC_Leboncoin.ProductModel], any Error> {
        .success([
            ProductModel(id: 1, categoryID: 1, title: "produit1", price: 200,imagesURL: ImagesURLModel(small: "url1") ,isUrgent: true),
            ProductModel(id: 2, categoryID: 2, title: "produit2", price: 200,imagesURL: ImagesURLModel(small: "url2") ,isUrgent: true),
            ProductModel(id: 3, categoryID: 3, title: "produit3", price: 200,imagesURL: ImagesURLModel(small: "url3") ,isUrgent: true),
            ProductModel(id: 4, categoryID: 3, title: "produit4", price: 200,imagesURL: ImagesURLModel(small: "url4") ,isUrgent: true),
        ])
    }
}

class CategoriesUseCaseMockSuccess: CategoriesUseCaseProtocol {
    func fetchCateogires() async -> Result<[POC_Leboncoin.CategoryModel], any Error> {
        .success([
            CategoryModel(id:1, name: "category1"),
            CategoryModel(id:2, name: "category2"),
            CategoryModel(id:3, name: "category3")
        ])
    }
}

class ProductsUseCaseMockFailed: ProductsUseCaseProtocol {
    func fetchProducts() async -> Result<[POC_Leboncoin.ProductModel], any Error> {
        .failure(NSError(domain: "", code: 0))
    }
}

class CategoriesUseCaseMockFailed: CategoriesUseCaseProtocol {
    func fetchCateogires() async -> Result<[POC_Leboncoin.CategoryModel], any Error> {
        .failure(NSError(domain: "", code: 0))
    }
}
