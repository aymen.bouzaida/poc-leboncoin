//
//  ProductTransformerTests.swift
//  POC-LeboncoinTests
//
//  Created by Aymen Bouzaida on 15/06/2024.
//

import XCTest
@testable import POC_Leboncoin

final class ProductTransformerTests: XCTestCase {

    // Subject under Test
    private var sut: ProductTransformer?
    
    // MARK: Life Cycle
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    // MARK: test Func buildCategoriesModel()
    func test_build_categories_Model() {
        // Given
        sut = ProductTransformer()
        // When
        let result = sut?.buildProductModel(json: [
            ProductJson(id: 1, categoryID: 1, title: "produit1", description: "description test 1", price: 200, imagesURL: ImagesURL(small: "usmallUrl1", thumb: "thumbUrl1"), creationDate: "2019-11-05T15:56:59+0000", isUrgent: true, siret: "siret1"),
            ProductJson(id: 2, categoryID: 2, title: "produit2", description: "description test 2", price: 200, imagesURL: ImagesURL(small: "usmallUrl2", thumb: "thumbUrl2"), creationDate: "2019-11-05T15:56:59+0000", isUrgent: true, siret: "siret2"),
            ProductJson(id: 3, categoryID: 3, title: "produit3", description: "description test 3", price: 200, imagesURL: ImagesURL(small: "usmallUrl3", thumb: "thumbUrl3"), creationDate: "2019-11-05T15:56:59+0000", isUrgent: true, siret: "siret3"),
            ProductJson(id: 4, categoryID: 4, title: "produit4", description: "description test 4", price: 200, imagesURL: ImagesURL(small: "usmallUrl4", thumb: "thumbUrl4"), creationDate: "2019-11-05T15:56:59+0000", isUrgent: true, siret: "siret4")
        ])
        // Then
        XCTAssertEqual(result?.first?.id, 1)
        XCTAssertEqual(result?.first?.title, "produit1")
        XCTAssertEqual(result?.first?.description, "description test 1")
        XCTAssertEqual(result?.first?.price, 200)
        XCTAssertEqual(result?.first?.imagesURL?.small, "usmallUrl1")
        XCTAssertEqual(result?.first?.imagesURL?.thumb, "thumbUrl1")
        XCTAssertEqual(result?.first?.creationDate, "2019-11-05T15:56:59+0000".formattedDate())
        XCTAssertEqual(result?.first?.isUrgent, true)
        XCTAssertEqual(result?.first?.siret, "siret1")
    }
}
