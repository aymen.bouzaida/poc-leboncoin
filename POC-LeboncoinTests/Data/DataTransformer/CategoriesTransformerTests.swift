//
//  CategoriesTransformer.swift
//  POC-LeboncoinTests
//
//  Created by Aymen Bouzaida on 14/06/2024.
//

import XCTest
@testable import POC_Leboncoin

final class CategoriesTransformerTests: XCTestCase {
    
    var sut: CategoriesTransformer?  // Subject under test
    
    // MARK: Life Cycle
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_buildCategoriesModel() {
        // Given
        sut = CategoriesTransformer()
        // When
        let result = sut?.buildCategoriesModel(json: [
            CategoryJson(id: 1, name: "category1"),
            CategoryJson(id: 2, name: "category2"),
            CategoryJson(id: 3, name: "category3")
        ])
        // Then
        XCTAssertNotNil(result)
        XCTAssertEqual(result?.count, 3)
        XCTAssertEqual(result?[0].id, 1)
        XCTAssertEqual(result?[1].id, 2)
        XCTAssertEqual(result?[2].id, 3)
        XCTAssertEqual(result?[0].name, "category1")
        XCTAssertEqual(result?[1].name, "category2")
        XCTAssertEqual(result?[2].name, "category3")

    }
}
