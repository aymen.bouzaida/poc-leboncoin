//
//  GetProductRepositoryTests.swift
//  POC-LeboncoinTests
//
//  Created by Aymen Bouzaida on 15/06/2024.
//

import XCTest
@testable import POC_Leboncoin

final class GetProductRepositoryTests: XCTestCase {

  // Subject Under Test
    private var sut: GetProductRepository?
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    // MARK: Func getProducts()
    func test_get_product_success() async {
        // Given
        sut = GetProductRepository(resources: NetworkMockProduct_Success())
        // When
        let result = await sut?.getProducts()
        // Then
        switch result {
        case .success(let products):
            XCTAssertEqual(products.last?.id, 4)
            XCTAssertEqual(products.last?.categoryID, 4)
            XCTAssertEqual(products.last?.title, "produit4")
            XCTAssertEqual(products.last?.price, 200)
            XCTAssertEqual(products.last?.imagesURL?.small, "usmallUrl4")
            XCTAssertEqual(products.last?.isUrgent, true)
        default:
            break;
        }
    }
    
    func test_get_product_failure() async {
        // Given
        sut = GetProductRepository(resources: NetworkMockProduct_Failure())
        // When
        let result = await sut?.getProducts()
        // Then
        switch result {
        case .failure(let error as NetworkErrorInterceptor):
            XCTAssertEqual(error.message, "No data found. Please try again")
        default:
            break;
        }
    }
}

class NetworkMockProduct_Success: Network {
    func downloadApiData<T>(_ resource: POC_Leboncoin.Resources<T>) async throws -> T where T : Decodable, T : Encodable {
        return [
            ProductJson(id: 1, categoryID: 1, title: "produit1", description: "description test 1", price: 200, imagesURL: ImagesURL(small: "usmallUrl1", thumb: "thumbUrl1"), creationDate: "2019-11-05T15:56:59+0000", isUrgent: true, siret: ""),
            ProductJson(id: 2, categoryID: 2, title: "produit2", description: "description test 2", price: 200, imagesURL: ImagesURL(small: "usmallUrl2", thumb: "thumbUrl2"), creationDate: "2019-11-05T15:56:59+0000", isUrgent: true, siret: ""),
            ProductJson(id: 3, categoryID: 3, title: "produit3", description: "description test 3", price: 200, imagesURL: ImagesURL(small: "usmallUrl3", thumb: "thumbUrl3"), creationDate: "2019-11-05T15:56:59+0000", isUrgent: true, siret: ""),
            ProductJson(id: 4, categoryID: 4, title: "produit4", description: "description test 4", price: 200, imagesURL: ImagesURL(small: "usmallUrl4", thumb: "thumbUrl4"), creationDate: "2019-11-05T15:56:59+0000", isUrgent: true, siret: "")
        ] as! T
    }
}

class NetworkMockProduct_Failure: Network {
    func downloadApiData<T>(_ resource: POC_Leboncoin.Resources<T>) async throws -> T where T : Decodable, T : Encodable {
        throw NetworkErrorInterceptor.noData
    }
}
