//
//  GetCategoriesRepositoryTests.swift
//  POC-LeboncoinTests
//
//  Created by Aymen Bouzaida on 14/06/2024.
//



import XCTest
@testable import POC_Leboncoin

final class GetCategoriesRepositoryTests: XCTestCase {
    
    var sut: GetCategoriesRepository?  // Subject under test
    
    // MARK: Life Cycle
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func test_getCategoires_success_case() async throws {
        // Given
        sut = GetCategoriesRepository(resources: NetworkMockCategoryJson())
        // When
        let result = await sut?.getCategoires()
        // Then
        switch result {
        case .success(let categoriesModel):
            XCTAssertEqual(categoriesModel.count, 3)
            XCTAssertEqual(categoriesModel.last?.name, "category3")
            XCTAssertEqual(categoriesModel.first?.id, 1)
        default:
            break;
        }
    }
    
    func test_getCategoires_failed_case() async throws {
        // Given
        sut = GetCategoriesRepository(resources: NetworkMockFailed())
        // When
        let result = await sut?.getCategoires()
        // Then
        switch result {
        case .failure(let error):
            let networkError = error as? NetworkErrorInterceptor
            XCTAssertEqual(networkError?.message, "Unknown error occurred. Please try again")
        default:
            break;
        }
    }
}


class NetworkMockCategoryJson: Network {
        
    func downloadApiData<T>(_ resource: POC_Leboncoin.Resources<T>) async throws -> T where T : Decodable, T : Encodable {
        return [
            CategoryJson(id: 1, name: "category1"),
            CategoryJson(id: 2, name: "category2"),
            CategoryJson(id: 3, name: "category3")

        ] as! T
    }
}


class NetworkMockFailed: Network {
        
    func downloadApiData<T>(_ resource: POC_Leboncoin.Resources<T>) async throws -> T where T : Decodable, T : Encodable {
        throw NetworkErrorInterceptor.unknownError
    }
}
